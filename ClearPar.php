<?php

class ClearPar{

  public function build($parentesis){

    preg_match('[(\(\))+]',$parentesis,$match);

    $output = implode('',$match);

    return $output;
  }

}

$clearPar = new ClearPar();
echo $clearPar->build('()())()').'<br>';
echo $clearPar->build('()(())').'<br>';

?>
