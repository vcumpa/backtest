<?php

class CompleteRange{
  public function build($numeros){
    $size = count($numeros);
    $i=0;
    for ($i=0; $i<$size-1; $i++) {
      $r1 = $numeros[$i];
      $r2 = $numeros[$i+1];
      //echo $r1.'-'.$r2.'<br>';
      foreach (range($r1,$r2) as $num) {
        array_push($numeros,$num);
      }
    }

    $complete = array_unique($numeros);
    sort($complete);
    //Retorno segun ejemplo no es array
    $output = '[';
    foreach ($complete as $num => $value) {
      $output .= $value.',';
    }
    $output = substr($output, 0, strlen($output)-1).']';

    return $output;
  }

}

$crearRango = new CompleteRange();
echo $crearRango->build([1,2,4,5]).'<br>';
echo $crearRango->build([55,58,60]);
?>
