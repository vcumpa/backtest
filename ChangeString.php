<?php

class ChangeString{

	public function build($cadena){
		$original = ('abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ');
		$match = ('bcdefghijklmnñopqrstuvwxyzaBCDEFGHIJKLMNÑOPQRSTUVWXYZA');
		return strtr($cadena,$original,$match);
	}

}

$cs = new ChangeString();
echo $cs->build("123 abcd*3").'<br>';
echo $cs->build("**Casa 52").'<br>';

?>
