<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';


//desde aqui

$app = new \Slim\App;
$container = $app->getContainer();

$container['view'] = new \Slim\Views\PhpRenderer("../templates/");

$app->get('/employees/', function ($request, $response, $args) {
  header('Location: ../employees');
  die();
});

$app->get('/employees', function ($request, $response, $args) {

    $file = file_get_contents(__DIR__ . '/../public/employees.json');
    $employee = json_decode($file);
    $findEmail = $request->getParam('email');

    if(isset($findEmail) && trim($findEmail)!=''){
      foreach ($employee as $key => $value) {
        $email = $employee[$key]->email;

        if($email==$findEmail){
          $empleado[] = $employee[$key];
        }
      }
    }else{
      $empleado = $employee;
    }

    $response = $this->view->render($response, "index.phtml", ["employee" => $empleado]);

    return $response;
});

$app->get('/employees/{id}', function ($request, $response, $args) {

    $file = file_get_contents(__DIR__ . '/../public/employees.json');
    $employee = json_decode($file);
    $findEmp = $args['id'];
    $empleado = '';
    if(isset($findEmp) && trim($findEmp)!=''){
      foreach ($employee as $key => $value) {
        $id = $employee[$key]->id;

        if($id==$findEmp){
          $empleado[] = $employee[$key];
        }
      }
    }

    $response = $this->view->render($response, "detail.phtml", ["employee" => $empleado]);

    return $response;
});

$app->get('/service', function ($request, $response, $args) {

    $file = file_get_contents(__DIR__ . '/../public/employees.json');
    $employee = json_decode($file);
    $min = $request->getParam('min');
    $max = $request->getParam('max');

    if(isset($min) && trim($min)!='' && isset($max) && trim($max)!='' && $max >= $min){
      foreach ($employee as $key => $value) {
        $clear = array('$',',');
        $empSalary = str_replace($clear,'',$employee[$key]->salary);
        $salary = floatval($empSalary);

        if($salary<=$max && $salary>=$min){
          $empleado[] = $employee[$key];
        }

        if(!isset($empleado) || count($empleado)==0){
          $empleado[] = array('Error' => 'No encontrado');
        }
      }
    }else{
      $empleado[] = array('Error' => 'Ingrese busqueda correctamente');
    }

    $renderer = new RKA\ContentTypeRenderer\Renderer();
    $response  = $renderer->render($request, $response, $empleado);

    return $response->withStatus(200);
});

// Run app
$app->run();
